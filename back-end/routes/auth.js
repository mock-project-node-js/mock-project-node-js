var express = require('express');
const authController = require('../controllers/authController');
const middleWareController = require('../middleware/middlewareController');
var router = express.Router();

//REGISTER
router.post('/register', authController.register);

//LOGIN
router.post('/login',authController.loginUser);

//REFRESH TOKEN
router.post('/refresh',authController.refreshToken);

//LOGOUT
router.post('/logout',middleWareController.verifyToken ,authController.userLogout);

//FORGOT PASSWORD 
router.post('/send-mail', authController.forgotPassword);
module.exports = router;