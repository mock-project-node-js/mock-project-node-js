var express = require('express');
var router = express.Router();
const userController = require('../controllers/userController')
const middleWareController =require('../middleware/middlewareController')
/* GET products listing. All products*/
router.get('/',middleWareController.verifyToken, userController.getAllProducts);

/* Delete products*/
router.delete('/delete/:id',middleWareController.checkRole, userController.deleteProducts);

/* Edit products*/
router.post('/edit/:id',middleWareController.checkRole, userController.editProduct);

/* Add new products*/
router.post('/addnew',middleWareController.checkRole, userController.addNewProduct);


module.exports = router;
