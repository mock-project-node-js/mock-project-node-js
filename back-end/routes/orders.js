var express = require('express');
var router = express.Router();
const orderController = require('../controllers/orderController')
const middleWareController =require('../middleware/middlewareController')
/* GET orders listing. All orders*/
router.get('/',middleWareController.verifyToken, orderController.getAllOrders);

/* Delete orders*/
router.delete('/delete/:id',middleWareController.verifyToken, orderController.deleteOrders);

/* Edit orders*/
router.post('/edit/:id',middleWareController.verifyToken, orderController.editOrders);

/* Add new orders*/
router.post('/addnew',middleWareController.verifyToken, orderController.createOrder);

/* search orders*/
router.get('/search/:keywords',middleWareController.verifyToken, orderController.searchOrders);

module.exports = router;