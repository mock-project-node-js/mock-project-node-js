const mongoose = require('mongoose');
const ordersSchema = new mongoose.Schema({
    name:{
        type:String,
        require:true,
    },
    numbers:{
        type:String,
        require:true,
    },
    totalPrice:{
        type:String,
        require:true,
        minlenght:6,
    },
    address:{
        type:String,
        require:true,
        minlenght:6,
    },
    productName:{
        type:String,
        required:true
    },
    phoneNumber:{
        type:String,
        required:true
    },
},
{timestamps:true},
{ collection: 'orders'}
)

//export mongoose interface
module.exports = mongoose.model("orders",ordersSchema);