const mongoose = require('mongoose');
const userSchema = new mongoose.Schema({
    userName:{
        type:String,
        require:true,
        minlenght:6,
        maxlenght:20,
        unique:true
    },
    email:{
        type:String,
        require:true,
        minlenght:6,
        maxlenght:20,
        unique:true
    },
    password:{
        type:String,
        require:true,
        minlenght:6,
    },
    admin:{
        type:Boolean,
        default:false
    }
},
{timestamps:true},
{ collection: 'users'}
)

//export mongoose interface
module.exports = mongoose.model("Users",userSchema);