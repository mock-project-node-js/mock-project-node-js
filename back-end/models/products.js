const mongoose = require('mongoose');
const productSchema = new mongoose.Schema({
    name:{
        type:String,
        require:true,
        unique:true
    },
    count:{
        type:String,
        require:true,
    },
    price:{
        type:String,
        require:true,
        minlenght:6,
    },
    img:{
        type:String,
        require:true,
        minlenght:6,
    },
    sale:{
        type:Boolean,
        default:false
    }
},
{timestamps:true},
{ collection: 'procutslists'}
)

//export mongoose interface
module.exports = mongoose.model("procutslists",productSchema);