const orders = require('../models/orders');

const ordersController = {
    //GET ALL ORDERS 
    getAllOrders: async(req,res)=>{
        try {
            const ordersList = await orders.find();
            res.status('200').json(ordersList);
        } catch (error) {
            res.status('500').json(error);
        }
    },

    //DELETE ORDERS
    deleteOrders: async(req,res)=>{
        try {
            //const user = await User.findByIdAndDelete(req.params.id);
            await orders.findByIdAndDelete(req.params.id);
            res.status('200').json('Delete successfuly');
        } catch (error) {
            res.status('500').json(error);
        }
    },
    //ADD ORDERS
    createOrder: async(req,res)=>{
        try {
            await orders.create(req.body);
            res.status('200').json('Create successfuly');
        } catch (error) {
            res.status('500').json(error);
        }
    },
    //EDIT ORDERS
    editOrders: async(req,res)=>{
        try {
            //const user = await User.findByIdAndDelete(req.params.id);
            //const user = await User.findByIdAndDelete(req.params.id);
            const product = await orders.findByIdAndUpdate(req.params.id);
            res.status('200').json('Upate successfuly');
        } catch (error) {
            res.status('500').json(error);
        }
    },
    //SEARCH ORDER
    searchOrders: async(req,res)=>{
        try {
            const objWhere ={};
            objWhere.name = req.query?.keywords||'';
            const ordersList = await orders.find(objWhere);
            res.status('200').json(ordersList);
        } catch (error) {
            res.status('500').json(error);
        }
    },

}

module.exports = ordersController;