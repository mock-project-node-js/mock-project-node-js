const products = require('../models/products');

const userController = {
    //GET ALL PRODUCTS
    getAllProducts: async(req,res)=>{
        try {
            // const page = req.query.page||1
            // const itemPerpage = (req.query.itemPerpage||10);
            // products.find({}).skip((itemPerpage * page) - perPage).limit(itemPerpage).exec((err, productsList) => {
            //     if (err) {
            //         console.log(err);
            //     }
            //     res.status('200').json(productsList);
            // }); 
            const productsList = await products.find();
            res.status('200').json(productsList);
        } catch (error) {
            res.status('500').json(error);
        }
    },

    //DELETE PRODUCT
    deleteProducts: async(req,res)=>{
        try {
            //const user = await User.findByIdAndDelete(req.params.id);
            await products.findByIdAndDelete(req.params.id);
            res.status('200').json('Delete successfuly');
        } catch (error) {
            res.status('500').json(error);
        }
    },
    //ADD PRODUCT
    addNewProduct: async(req,res)=>{
        try {
            await products.create(req.body);
            res.status('200').json('Create successfuly');
        } catch (error) {
            res.status('500').json(error);
        }
    },
    //EDIT PRODUCT
    editProduct: async(req,res)=>{
        try {
            //const user = await User.findByIdAndDelete(req.params.id);
            //const user = await User.findByIdAndDelete(req.params.id);
            const product = await products.findByIdAndUpdate(req.params.id);
            res.status('200').json('Upate successfuly');
        } catch (error) {
            res.status('500').json(error);
        }
    },

}

module.exports = userController;