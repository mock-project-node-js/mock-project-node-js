//import
const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const User = require("../models/Users");
const mailer = require("../utils/mailer");
const mailConfig = require("../config/mail.config");
let refreshTokenList = [];
const authController = {
  //REGISTER
  register: async (req, res) => {
    try {
      const salt = await bcrypt.genSalt(10);
      const hashed = await bcrypt.hash(req.body.password, salt);

      //Create new user
      const newUser = await new User({
        userName: req.body.userName,
        email: req.body.email,
        password: hashed,
      });

      //save to data base
      const user = await newUser.save();
      res.status(200).json(user);
    } catch (error) {
      console.log(error);
      res.status("500").json(error);
    }
  },
  // GENERATE ACCESS TOKEN
  generateAccessToken: (user) => {
    return jwt.sign(
      {
        id: user.id,
        admind: user.admin,
      },
      process.env.JWT_ACCESS_KEY,
      { expiresIn: "3h" }
    );
  },

  //GENERATE REFRESH TOKEN
  generateRefreshToken: (user) => {
    return jwt.sign(
      {
        id: user.id,
        admind: user.admin,
      },
      process.env.JWT_REFESH_KEY,
      { expiresIn: "1d" }
    );
  },
  //LOGIN
  loginUser: async (req, res) => {
    try {
      const user = await User.findOne({ userName: req.body.userName });
      if (!user) {
        res.status("404").json("Wrong user name or password ! :(((");
      }
      const validPassword = await bcrypt.compare(
        req.body.password,
        user.password
      );
      if (!validPassword) {
        res.status("404").json("Wrong user name or password ! :(((");
      }
      if (user && validPassword) {
        const accessToken = authController.generateAccessToken(user);
        const refreshToken = authController.generateRefreshToken(user);
        refreshTokenList.push(refreshToken);
        res.cookie("refreshToken", refreshToken, {
          httpOnly: true,
          secure: false,
          path: "/",
          sameSite: "strict",
        });
        const { password, ...other } = user._doc;
        res.status("200").json({ ...other, accessToken });
      }
    } catch (error) {
      res.status("500").json(error);
    }
  },

  //REFRESH TOKEN
  refreshToken: async (req, res) => {
    //get refresh token from user
    const refreshToken = req.cookies.refreshToken;
    if (!refreshToken) {
      return res.status(401).json("you're not authenticated!");
    }
    if (!refreshTokenList.includes(refreshToken)) {
      return res.status(403).json("Refresh token is not valid!");
    }
    jwt.verify(refreshToken, process.env.JWT_REFESH_KEY, (err, user) => {
      if (err) {
        console.log(err);
      }

      refreshTokenList = refreshTokenList.filter(
        (token) => token !== refreshToken
      );
      //gen new token
      const newAccessToken = authController.generateAccessToken(user);
      const newRefeshToken = authController.generateRefreshToken(user);
      refreshTokenList.push(newRefeshToken);
      res.cookie("refreshToken", newRefeshToken, {
        httpOnly: true,
        secure: false,
        path: "/",
        sameSite: "strict",
      });
      res.status(200).json({ accessToken: newAccessToken });
    });
  },

  //LOGOUT
  userLogout: async (req, res) => {
    res.clearCookie("refreshToken");
    refreshTokenList = refreshTokenList.filter(
      (token) => token !== req.cookies.refreshToken
    );
    res.status(200).json("log out success");
  },

  //FOR GOT PASS WORD
  forgotPassword: async (req, res) => {
    try {
       const {email} = req.body;
       const user = await User.findOne({ email: email });
      if (!user) {
        return res.status("404").json("Wrong user email ! :(((");
      }
      const data= {
        from:mailConfig.FROM_ADDRESS,
        to:email,
        subject:'Account activation link',
        html:`
        <h1>Please click here to reset password</h1>
        <p>Click here <a href="http://localhost:3000">Bankery Shop</a></p> `
      }
       await mailer.sendMail(data.to, data.subject,data.html);
       // Quá trình gửi email thành công thì gửi về thông báo success cho người dùng
        res.send('<h3>Your email has been sent successfully.</h3>')
      } catch (error) {
        // Nếu có lỗi thì log ra để kiểm tra và cũng gửi về client
        console.log(error)
        res.send(error)
      }
  },
};

//STORE TOKEN
// 1) LOCAL STORAGE
// easy to be attacked XSS
// 2) COOKIES: -> HTTPONLY COOKIES
// hard to be attacked XSS but easy for CSRF -> SAME SITE
// 3) REDUX STORE -> SAVE ACCESSTOKEN
// HTTPONLY -> SAVE REFESH-TOKEN
// BFF PATREN -> BACKEND FOR FRONTEND PATREN
module.exports = authController;
