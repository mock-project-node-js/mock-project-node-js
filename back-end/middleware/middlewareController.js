const jwt = require("jsonwebtoken");

const middleWareController = {

    //verifyToken
    verifyToken:(req,res,next)=>{
        const token = req.headers.authorization;
        if (token) {
            //Bearer token
            const accessToken = token.split(" ")[1]; 
            jwt.verify(accessToken,process.env.JWT_ACCESS_KEY,(err,user)=>{
                if (err) {
                    res.status(403).json("TOKEN is not valid");
                }else{
                    req.user = user;
                    next();
                }
            })
        }else{
            res.status(401).json("you're not authenticated")
        }
    },

    //verify token and admin
    checkRole:(req,res,next)=>{
        middleWareController.verifyToken(req,res,()=>{
            if (req.user.id === req.params.id || req.user.admind) {
                next();
            }else{
                res.status(403).json("you are not allowed to delete this user !")
            }
        })
    }
}

module.exports = middleWareController