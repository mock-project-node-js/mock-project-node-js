const nodeMailer = require('nodemailer');
const mailConfig = require('../config/mail.config');
require('dotenv/config');

exports.sendMail = (to, subject, htmlContent) => {
    const transport = nodeMailer.createTransport({
        //name: mailConfig.APP_URL,
        host: mailConfig.HOST,
        port: mailConfig.PORT,
        service: "Gmail",
        secure: true,
        auth: {
            user: mailConfig.USERNAME,
            pass: mailConfig.PASSWORD,
        }
    })

    const options = {
        from: mailConfig.FROM_ADDRESS,
        to: to,
        subject: subject,
        html: htmlContent
    }
    return transport.sendMail(options,(error,res)=>{
        if (error) {
            // res.send("Email could not send due to error" +error);
            return console.log(error);
        }
        else{
            return console.log('tlsv',res);
        }
    });
}