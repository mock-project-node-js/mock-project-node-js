import React from 'react';
import './App.css';
import { BrowserRouter as Router, Navigate, Route, Routes} from "react-router-dom";
import HomePage from './Components/HomePage/HomePage';
import Login from './Components/Login/Login';
import Register from './Components/Register/Register';
import ForgotPassword from './Components/ForgotPassword/ForgotPassword';
import AddProduct from './Components/AddProduct/addProduct';

function App() {
  return (
    <Router>
      <div className="App"> 
        <Routes>
          <Route path ="/" element={<Navigate to = "/login"/>}/>
          <Route path="/home" element={<HomePage />} />
          <Route path="/login" element={ <Login />} />
          <Route path="/register" element={<Register />} />
          <Route path="/forgot-password" element={<ForgotPassword />} />
          <Route path="/add-product" element={<AddProduct />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;