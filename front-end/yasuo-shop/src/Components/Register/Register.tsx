import { useState } from "react";
import { useDispatch } from "react-redux";
import { useNavigate } from "react-router-dom";
import { registerUser } from "../../redux/apiRequest";
import "./register.css";

 
export default function Register() {
  const [email, setEmail]= useState("");
  const [userName, setUserName]= useState("");
  const [passWord, setPassword]= useState("");
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const handleRegidter = (e: any)=>{
    e.preventDefault();
    const newLoginUser = {
      userName:userName,
      password:passWord,
      email:email
    };
    registerUser(newLoginUser,dispatch,navigate);
  }
    return (
        <div className="flex flex-col justify-center md:justify-center w-full h-screen register-container">
        <form onSubmit={handleRegidter} className="max-w-[400px] w-full mx-auto bg-amber-600 p-8 px-8 rounded-lg">
          <h2 className="text-4xl text-white font-bold text-center"> SIGN UP:</h2>
          <div className="flex flex-col text-white py-2">
            <label>User name:</label>
            <input type="text" name="" id="" className="rounded-lg bg-amber-100 mt-2 p-2 focus:border-blue-500 focus:outline-none focus:bg-gray-300 text-amber-800" placeholder="Enter user name" onChange={(e)=>{setUserName(e.target.value)}}/>
          </div>
          <div className="flex flex-col text-white py-2">
            <label>Email:</label>
            <input type="email" name="" id="" className="rounded-lg bg-amber-100 mt-2 p-2 focus:border-blue-500 focus:outline-none focus:bg-gray-300 text-amber-800" placeholder="Enter email" onChange={(e)=>{setEmail(e.target.value)}}/>
          </div>
          <div className="flex flex-col text-white py-2">
            <label>Password:</label>
            <input type="password" name="" id="" className="rounded-lg bg-amber-100 mt-2 p-2 focus:border-blue-500 focus:outline-none focus:bg-gray-300 text-amber-800" placeholder="Enter password" onChange={(e)=>{setPassword(e.target.value)}}/>
          </div>
          <div className="flex flex-col text-white py-2">
            <label>Repeat Password:</label>
            <input type="password" name="" id="" className="rounded-lg bg-amber-100 mt-2 p-2 focus:border-blue-500 focus:outline-none focus:bg-gray-300 text-amber-800" placeholder="Repeat password"/>
          </div>
          <div className="flex justify-between text-gray-50 py-2">
            <p className="flex items-center">
              <input type="checkbox" name="save-acc" id="save-acc" className="mr-2 cursor-pointer mt-1 accent-amber-800"/>
              <label htmlFor="save-acc" className="cursor-pointer hover:text-blue-800"> Remember me</label>
            </p>
          </div>
          <button className="w-full my-5 py-2 bg-amber-800 shadow-lg shadow-amber-800/80 hover:shadow-amber-800/60 text-white font-semibold rounded-lg">Sign up</button>
        </form>
      </div>
    )
};