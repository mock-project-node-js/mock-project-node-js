import { useState } from "react";
import { useDispatch } from "react-redux";
import { Link, useNavigate } from "react-router-dom";
import { loginUser } from "../../redux/apiRequest";
import "./forgotPassword.css";


export default function FotgotPassword() {
  const [email, setEmail]= useState("");
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const handleLogin = (e: any)=>{
    e.preventDefault();
    const forgotPassword = {
      email:email,
    };
    console.log(forgotPassword);
    
  }

  return (
      <div className="flex flex-col justify-center md:justify-center w-full h-screen login-content">
        <form onSubmit={handleLogin} className="max-w-[400px] w-full mx-auto bg-amber-600 p-8 px-8 rounded-lg">
          <h2 className="text-4xl text-white font-bold text-center"> FORGOT PASS:</h2>
          <div className="flex flex-col text-white py-2">
            <label>Email:</label>
            <input type="text" name="" id="" className="rounded-lg bg-amber-100 mt-2 p-2 focus:border-blue-500 focus:outline-none focus:bg-gray-300 text-amber-800" placeholder="Enter email" onChange={(e)=>{setEmail(e.target.value)}}/>
          </div>
          {/* <div className="flex flex-col text-white py-2">
            <label>Password:</label>
            <input type="password" name="" id="" className="rounded-lg bg-amber-100 mt-2 p-2 focus:border-blue-500 focus:outline-none focus:bg-gray-300 text-amber-800" placeholder="Enter password" onChange={(e)=>{setPassword(e.target.value)}}/>
          </div>
          <div className="flex justify-between text-gray-50 py-2">
            <p className="flex items-center">
              <input type="checkbox" name="save-acc" id="save-acc" className="mr-2 cursor-pointer mt-1 accent-amber-800"/>
              <label htmlFor="save-acc" className="cursor-pointer hover:text-blue-800"> Remember me</label>
            </p>
            <p className="cursor-pointer hover:text-blue-800">Forgot your password ?</p>
          </div>
          <button className="w-full my-5 py-2 bg-amber-800 shadow-lg shadow-amber-800/80 hover:shadow-amber-800/60 text-white font-semibold rounded-lg">Sign in</button>
          <div className="flex items-center text-gray-50 py-2">
            <span>
              Don't have any account ? <Link to="/register" className="hover:text-blue-800">Sign up</Link>
            </span>
          </div> */}
          <button className="w-full my-5 py-2 bg-amber-800 shadow-lg shadow-amber-800/80 hover:shadow-amber-800/60 text-white font-semibold rounded-lg">SEND</button>
        </form>
      </div>
  );
}
