import "./home.css";
import Navbar from "../Navbar/Navbar";
import { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { getListProDuct } from "../../redux/apiRequest";
import { useNavigate } from "react-router-dom";

const HomePage = () => {
  const user = useSelector((state: any) => state.auth.login.currentUser);
  const dispatch = useDispatch();
  let lit:any;
  const navigate = useNavigate();
  const getListProduct123 = async (token:any)=>{
    lit= await getListProDuct(token||'',dispatch, navigate);
  }
  //useEffect
  useEffect(() => {
    getListProduct123(user?.accessToken);
  });
  return (
    <main className="home-container">
      <Navbar></Navbar>
      <div className="home-title">User List</div>
      <div className="home-userlist">
      {[...lit].map((item, i) =><div>
        <p>{item.name}</p>
      </div>)}
      </div>
    </main>
  );
};

export default HomePage;
