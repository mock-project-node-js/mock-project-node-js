import { createSlice } from "@reduxjs/toolkit";

const authSlice = createSlice({
    name: "auth",
    initialState: {
        login: {
            currentUser: null,
            isFetching: false,
            error: false,
        },
        register:{
            success: false,
            isFetching: false,
            error: false,
        }
    },
    reducers: {
        loginStart:(state)=>{
            state.login.isFetching=true;
        },
        loginSuccess: (state, action)=>{
            state.login.isFetching=false;
            state.login.currentUser= action.payload;
            state.login.error = false
        },
        loginFailse: (state)=>{
            state.login.isFetching= false;
            state.login.error = true;
        },
        registerStart:(state)=>{
            state.register.isFetching=true;
        },
        registerSuccess: (state, action)=>{
            state.register.isFetching=false;
            state.register.success= true;
            state.register.error = false
        },
        registerFailse: (state)=>{
            state.register.isFetching= false;
            state.register.error = true;
            state.register.success= false;
        }
    }
});

export const {
    loginStart,
    loginFailse,
    loginSuccess,
    registerStart,
    registerSuccess,
    registerFailse
} = authSlice.actions

export default authSlice.reducer;
