import axios from "axios";
import { loginFailse, loginStart, loginSuccess, registerFailse, registerStart, registerSuccess } from "./authSlice";

export const loginUser = async(user: any, dispatch: (arg0: { payload: any; type: string; }) => void, navigate: (arg0: string) => void) =>{
    dispatch(loginStart());
    try{
        const res = await axios.post("http://localhost:3001/v1/auth/login",user);
        dispatch(loginSuccess((await res).data));
        navigate("/home")
    }catch(err){
        dispatch(loginFailse());
    }
}

export const registerUser = async(user: any, dispatch: (arg0: { payload: any; type: string; }) => void, navigate: (arg0: string) => void) =>{
    dispatch(registerStart());
    try{
        const res = await axios.post("http://localhost:3001/v1/auth/register",user);
        dispatch(registerSuccess((await res).data));
        navigate("/login")
    }catch(err){
        dispatch(registerFailse());
    }
}

export const logOut = async(token: any, dispatch: (arg0: { payload: any; type: string; }) => void, navigate: (arg0: string) => void) =>{
    dispatch(registerStart());
    try{
        axios.defaults.headers.common['Authorization'] = "Bearer " + token
        const res = await axios.post("http://localhost:3001/v1/auth/logout",{});
        dispatch(registerSuccess((await res).data));
        navigate("/login")
    }catch(err){
        dispatch(registerFailse());
    }
}

export const getListProDuct = async(token: any, dispatch: (arg0: { payload: any; type: string; }) => void, navigate: (arg0: string) => void) =>{
    dispatch(registerStart());
    try{
        axios.defaults.headers.common['Authorization'] = "Bearer " + token
        const res = await axios.get("http://localhost:3001/v1/user");
        dispatch(registerSuccess((await res).data));
        //navigate("/login")
    }catch(err){
        console.log(err);
        
    }
}